window.addEventListener('load',()=>{
    game = new Phaser.Game( width,height, Phaser.AUTO, 'canvas');
    game.state.add('play', playState);
    game.state.add('boot', bootState);
    game.state.add('load', loadState);
    game.state.add('spec',specState);
    game.state.add('menu',menuState);
    game.state.start('boot');
},false);