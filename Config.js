var game;
var a=1;
const height = 650;
const width = 500;
const regularEnemyState = 0;
const waitState = 1;
const bossState = 2;
var score=0;
//weapons
var enemyWeapon0 = {
    bulletPool:100,
    bulletPic:'enemyBullet0',
    bulletSpeed:300,
    //敵機們共用同一個bulletPool。這導致了頻率若太低的話，每秒能夠射擊出來子彈數目會被限制，所以乾脆調成0
    fireRate:0,
    fireAngle:Phaser.ANGLE_DOWN,
    attackType:0,
    positions:null,
    fireLimit:null,
}
var Weapon0 = {
    bulletPool:100,
    bulletPic:'bullet0',
    
    bulletSpeed:600,
    fireRate:200,
    fireAngle:Phaser.ANGLE_UP,
    attackType:0,
    positions:null,
    fireLimit:null
}
var Weapon1 = {
    bulletPool:100,
    bulletPic:'bullet0',
    isAnimation:false,
    bulletSpeed:600,
    fireRate:400,
    fireAngle:Phaser.ANGLE_UP,
    attackType:1,
    positions:[
            { x: 0, y: -32 },
            { x: -16, y: -16 },
            { x: 16, y: -16 },
            { x: -32, y: 0 },
            { x: 0, y: 0 },
            { x: 32, y: 0 }
    ],
    fireLimit:null
}
var Weapon2 = {
    bulletPool:100,
    bulletPic:'bullet0',
    isAnimation:false,
    bulletSpeed:600,
    fireRate:200,
    fireAngle:Phaser.ANGLE_UP,
    attackType:2,
    positions:[
            { x:Math.cos(240*0.017453293), y:Math.sin(240*0.017453293)},
            { x:Math.cos(243*0.017453293), y:Math.sin(243*0.017453293)},
            { x:Math.cos(246*0.017453293), y:Math.sin(246*0.017453293)},
            { x:Math.cos(249*0.017453293), y:Math.sin(249*0.017453293)},
            { x:Math.cos(252*0.017453293), y:Math.sin(252*0.017453293)},

            { x: Math.cos(264*0.017453293), y:Math.sin(264*0.017453293)},
            { x: Math.cos(267*0.017453293), y:Math.sin(267*0.017453293)},
            { x:Math.cos(270*0.017453293), y:Math.sin(270*0.017453293)},
            { x:Math.cos(273*0.017453293), y:Math.sin(273*0.017453293)},
            { x:Math.cos(276*0.017453293), y:Math.sin(276*0.017453293)},

            { x:Math.cos(288*0.017453293), y:Math.sin(288*0.017453293)},
            { x:Math.cos(291*0.017453293), y:Math.sin(291*0.017453293)},
            { x:Math.cos(294*0.017453293), y:Math.sin(294*0.017453293)},
            { x:Math.cos(297*0.017453293), y:Math.sin(297*0.017453293)},    
            { x:Math.cos(300*0.017453293), y:Math.sin(300*0.017453293)},
            //{x:-5,y:-6}
    ],
    fireLimit:null
}
var Weapon3 = {
    bulletPool:100,
    bulletPic:'bullet0',
    isAnimation:false,
    bulletSpeed:2000,
    fireRate:150,
    fireAngle:Phaser.ANGLE_UP,
    attackType:3,
    positions:null,
    fireLimit:null
}
var Weapon4 = {
    bulletPool:1,
    bulletPic:'nivose',
    isAnimation:true,
    frame:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],
    bulletSpeed:70,
    fireRate:150,
    fireAngle:Phaser.ANGLE_UP,
    attackType:4,
    positions:null,
    fireLimit:1
}
//boss
var boss ={
    windowWidth:width,
    windowHeight:height,
    initPosX:width/2,            
    initPosY:-100,
    bossPic:'boss',
    isAnimation:true,
    animation:{
        name:'bossFly',
        frame:[0,1],                
        repeatPerSec:20,
        isRepeat:true
    },
    physicSize:{
        width:232,
        height:232,
        offSetX:12,
        offSetY:12
    },
    moveInterval:2,
    speed:4,
    weapon:null,
    expPic:'bossExp',
    expBgm:'expBgm',
    gunFire:'gunFire',
    point:20000
}
//enemys
var enemy0 = {
    windowWidth:width,
    windowHeight:height,
    enemyPic:'enemy0',
    isAnimation:true,
    animation:{
        name:'enemyFly0',
        frame:[0,1,2],
        repeatPerSec:20,
        isRepeat:true
    },
    velocity:{
        x:{
            min:-60,
            max:60
        },
        y:{
            min:100,
            max:150,
        }
    },
    enemyPool:20,
    timeInterval:3.3,
    bulletTimeInterval:1100,
    health:3,
    weapon:null,
    expPool:30,
    expPic:'exp',
    emitterPic:'pixel',
    expBgm:'expBgm',
    gunFire:'gunFire',
    maxAmount:5,
    voice:null,
    point:500
}
var suicidePlane = {
    windowWidth:width,
    windowHeight:height,
    enemyPic:'enemy0',
    isAnimation:true,
    animation:{
        name:'suicideFly',
        frame:[0,1,2],
        repeatPerSec:20,
        isRepeat:true
    },
    velocity:{
        x:{
            min:0,
            max:0
        },
        y:{
            min:350,
            max:400,
        }
    },
    enemyPool:5,
    timeInterval:5,
    bulletTimeInterval:2500,
    health:8,
    weapon:null,
    expPool:10,
    expPic:'exp',
    emitterPic:'pixel',
    expBgm:'expBgm',
    gunFire:null,
    maxAmount:5,
    voice:[
        'fly',
        'banzai'
    ],
    point:2000
}
//players
var player0 ={
    windowWidth:width,
    windowHeight:height,
    initPosX:width/4,            
    initPosY:height*4/5,
    playerPic:'player0',
    isAnimation:true,
    animation:{
        name:'player0Fly',
        frame:[0,1,2],                
        repeatPerSec:20,
        isRepeat:true
    },
    physicSize:{
        width:40,
        height:40,
        offSetX:12,
        offSetY:12
    },
    speed:4,
    fireButton:Phaser.KeyCode.SPACEBAR,
    cursors:{
        up:Phaser.KeyCode.W,
        down:Phaser.KeyCode.S,
        left:Phaser.KeyCode.A,
        right:Phaser.KeyCode.D
    },
    weapon:null,
    expPic:'exp',
    emitterPic:'pixel',
    expBgm:'expBgm',
    gunFire:'gunFire',
    nivoseButton:Phaser.KeyCode.F
}

var player1 ={
    windowWidth:width,
    windowHeight:height,
    initPosX:width*3/4,            
    initPosY:height*4/5,
    playerPic:'player1',
    isAnimation:false,
    animation:{
        name:'player1Fly',
        frame:null,                
        repeatPerSec:20,
        isRepeat:true
    },
    physicSize:{
        width:40,
        height:40,
        offSetX:12,
        offSetY:12
    },
    speed:4,
    fireButton:Phaser.KeyCode.NUMPAD_0,
    cursors:{
        up:Phaser.KeyCode.UP,
        down:Phaser.KeyCode.DOWN,
        left:Phaser.KeyCode.LEFT,
        right:Phaser.KeyCode.RIGHT
    },
    weapon:null,
    expPic:'exp',
    emitterPic:'pixel',
    expBgm:'expBgm',
    gunFire:'gunFire',
    nivoseButton:Phaser.KeyCode.NUMPAD_1
}