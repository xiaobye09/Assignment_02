var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        game.stage.backgroundColor = '#ffffff';
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#000000' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        
        //load game resource
        game.load.spritesheet('player0','./assets/img/player.png',64,64);
        game.load.spritesheet('player1','./assets/img/player1.png');
        game.load.image('bullet0', './assets/img/bullet.png');
        game.load.spritesheet('enemy0','assets/img/enemy0.png',32,32);
        game.load.image('enemyBullet0', './assets/img/bullet.png');
        game.load.image('pixel','./assets/img/pixel.png');
        game.load.spritesheet('exp','./assets/img/exp.png',50,50);
        game.load.audio('bgm0', './assets/music/bgm0.mp3');
        game.load.audio('expBgm','./assets/music/expBgm.wav');
        game.load.audio('gunFire','./assets/music/gunFire.wav');
        game.load.spritesheet('btn', 'assets/img/but.png', 192, 48);
        game.load.image('background', './assets/img/background.png');
        game.load.audio('banzai','./assets/music/banzai.mp3');
        game.load.audio('fly','./assets/music/fly.wav');
        game.load.spritesheet('boss','./assets/img/boss.png',256,256);
        game.load.spritesheet('bossExp','./assets/img/bossexp.png',250,250);
        game.load.spritesheet('nivose','./assets/img/nivose.png',250,250);
        game.load.image('spec', './assets/img/spec.png');
    },
    create: function() {
        // Go to the menu state
        game.state.start('menu');
    } 
        
}