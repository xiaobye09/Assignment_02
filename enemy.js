function ENEMY(config){
    this.init=function(){
        this.enemyCnt = 0;
        //game window width height
        this.gameWidth = config.windowWidth;
        this.gameHeight = config.windowHeight;
        //enemy attribute
        this.width = game.cache.getImage(config.enemyPic).width;
        this.height = game.cache.getImage(config.enemyPic).heigth;
        this.isAnimation = config.isAnimation;
        this.animationName = config.animation.name;
        this.velocity={
            xMin:config.velocity.x.min, 
            xMax:config.velocity.x.max,
            yMin:config.velocity.y.min,
            yMax:config.velocity.y.max
        }
        this.health = config.health;
        //enemy pool
        this.enemys = game.add.group();
        this.enemys.enableBody = true;
        this.enemys.createMultiple(config.enemyPool*a, config.enemyPic);
        this.enemys.setAll('anchor.x', 0.5);
        this.enemys.setAll('anchor.y', 0.5);
        this.enemys.setAll('outOfBoundsKill', true);
        this.enemys.setAll('checkWorldBounds', true);
        if(this.isAnimation==true){
            this.enemys.forEach(function(enemy) {
                enemy.animations.add(config.animation.name,config.animation.frame,config.animation.repeatPerSec, config.animation.isRepeat);
            });
        };
        //timer
        this.loop = game.time.events.loop(Phaser.Timer.SECOND*config.timeInterval/a,this.generateEnemy,this);
        this.loop.timer.start();
        //console.log('enemy init')
        //enemy bullet
        this.weapon = config.weapon;
        if(this.weapon!=null)
            this.weapon.bulletPool.bulletSpeed*=a;

        this.bulletTimeInterval = config.bulletTimeInterval/a;
        //enemy explosion
        if(config.expPic!=null){
            this.exps = game.add.group();
            this.exps.createMultiple(config.expPool,config.expPic);
            this.exps.forEach(function(exp){
                exp.animations.add(config.expPic);
            });
        }
        //particle emitter
        if(config.emitterPic!=null){
            this.emitter=game.add.emitter(0,0,20);
            this.emitter.makeParticles(config.emitterPic);
            this.emitter.setYSpeed(-50,50);
            this.emitter.setXSpeed(-50,50);
            this.emitter.setScale(1,0,1,0,1000);
            this.emitter.gravity =0;
        }
        //audio
        if(config.expBgm!=null){
            this.expBgm=game.add.audio('expBgm');
        }
        if(config.gunFire!=null){
            this.gunFire = game.add.audio('gunFire');
            this.gunFire.volume = 0.1;
        }
        if(config.voice){
            this.voice = [];
            for(i=0;i<config.voice.length;++i){
                this.voice.push(game.add.audio(config.voice[i]))
                this.voice[i].volume = 0.1;
            }
        }
            
    }
    //member functions
    this.generateEnemy = function(){
        let vel = this.velocity;    
        var enemy = this.enemys.getFirstExists(false);
        if(enemy){
            if(config.voice){
                for(i=0;i<this.voice.length;++i){
                    try{
                        this.voice[i].play();
                    }catch{} 
                }
            }
            enemy.reset(game.rnd.integerInRange(this.width ,this.gameWidth-this.width), 0);
            enemy.body.velocity.x = game.rnd.integerInRange(vel.xMin,vel.xMax);
            enemy.body.velocity.y = game.rnd.integerInRange(vel.yMin,vel.yMax);
            enemy.health = this.health;
            if(this.isAnimation){
                enemy.play(this.animationName);
            }
        }
        this.enemyCnt++;
        //console.log(this.enemyCnt+"隻")
    };
    this.fire = function(){
        //console.log('test fire');
        if(this.weapon!=null){
            let time = game.time.now;
            this.enemys.forEachExists((enemy)=>{
                if(time>(enemy.bulletTime||0)){
                    this.weapon.bulletPool.trackSprite(enemy);
                    enemy.bulletTime =time+this.bulletTimeInterval; 
                    try{
                        if(this.weapon.fire())
                            this.gunFire.play();
                    }catch{}
                }
            },this);
        }
    };
    this.hitEnemy = function(bullet,enemy){
        bullet.kill();
        enemy.health--;
        if(enemy.health<=0){
            score=score+config.point;
            enemy.kill();
            try{
                let exp = this.exps.getFirstExists(false);
                exp.reset(enemy.body.x,enemy.body.y);
                exp.play(config.expPic,16,false,true);
                this.expBgm.play();
                this.emitter.x=enemy.body.x+enemy.body.halfWidth;
                this.emitter.y=enemy.body.y+enemy.body.halfHeight;
                game.camera.shake(0.01, 200);
                this.emitter.start(true,1000,null,20);
            }catch{}
        }
    };
    this.expired = function(){
        if(config.maxAmount<=this.enemyCnt){
            game.time.events.remove(this.loop); 
            console.log('enemy stop');
            return true;
        }else{
            return false;
        }
    };
    this.pickClosestTo=function(player){
        let min = 10000;
        let min_e=null;
        let group = this.enemys.getAll( 'alive',true);
        for(let i=0;i<group.length;i++){
            let len = game.math.distance(player.centerX,player.centerY,group[i].centerX,group[i].centerY);
            if(len<min){
                min_e = group[i];
            }
        }
        if(min_e!=null){
            return min_e;
        }else{
            return null;
        }
    },
    this.nivoseHit = function(nivose,enemy){
        enemy.health=enemy.health-10;
        if(enemy.health<=0){
            score=score+config.point;
            enemy.kill();
            try{
                let exp = this.exps.getFirstExists(false);
                exp.reset(enemy.body.x,enemy.body.y);
                exp.play(config.expPic,16,false,true);
                this.expBgm.play();
                this.emitter.x=enemy.body.x+enemy.body.halfWidth;
                this.emitter.y=enemy.body.y+enemy.body.halfHeight;
                game.camera.shake(0.01, 200);
                this.emitter.start(true,1000,null,20);
            }catch{}
        }
    }
}
/*
config = {
    windowWidth:number,
    windowHeight:number,
    enemyPic:string,
    isAnimation:boolean,
    animation:{
        name:string,
        frame:frame,
        repeatPerSec:number,
        isRepeat:boolean
    },
    velocity:{
        x:{
            min:number;
            max:number;
        },
        y:{
            min:number;
            max:number;
        }
    },
    enemyPool:number,
    timeInterval:number,
    weapon:WEAPON,
    expPool:number,
    expPic:string
}*/


   /*  enemiesHit: function(bullet, enemy) {
        bullet.kill();
        enemy.kill();
        console.log(this.enemyPool0.countDead());
        /*var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
    },
*/