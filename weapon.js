function WEAPON(config){
    this.init=function(){
        //bullet pool
        this.bulletPool = game.add.weapon(config.bulletPool,config.bulletPic);
        if(config.fireLimit!=null)
            this.bulletPool.resetShots(config.fireLimit);
        this.bulletPool.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.bulletPool.multiFire=true;
        this.bulletPool.bulletSpeed = config.bulletSpeed;
        this.bulletPool.fireRate = config.fireRate;
        this.bulletPool.fireAngle= config.fireAngle;
        if(config.isAnimation){
            this.bulletPool.addBulletAnimation(config.bulletPic+'fly', config.frame,130,true);
        }
        //fire positions
        this.positions = config.positions;
    }
    this.fire=function(target){
        if(config.attackType==0){
            return this.bulletPool.fire();
        }else if(config.attackType==1){
            this.bulletPool.fireMany(this.positions);
            return true;
        }else if(config.attackType ==2){
            
            for(let i=0;i<config.positions.length;++i){
                let trackedSprite = this.bulletPool.trackedSprite;
                let x = config.positions[i].x;
                let y = config.positions[i].y;
                this.bulletPool.fire({x:trackedSprite.centerX,y:trackedSprite.centerY}
                                    ,trackedSprite.centerX+x
                                    ,trackedSprite.centerY+y);
            }
            return true;
        }else if(config.attackType ==3){
            if(target==null){
                return this.bulletPool.fire();
            }else{
                return this.bulletPool.fireAtSprite(target);
            }
            
        }else if(config.attackType==4){
            return this.bulletPool.fire();
        }
    };
    return false;
}
/*
weapon:{
    bulletPool:number,
    bulletPic:string,
    bulletSpeed:number,
    fireRate:number,
    fireAngle:???,
    positions:[
            { x: 0, y: -32 },
            { x: -16, y: -16 },
            { x: 16, y: -16 },
            { x: -32, y: 0 },
            { x: 0, y: 0 },
            { x: 32, y: 0 }
    ],
}
*/
