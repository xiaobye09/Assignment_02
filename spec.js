var specState ={
    create:function(){
        this.spec = game.add.sprite(0,0,'spec');
        this.spec.inputEnabled = true;
        this.spec.events.onInputDown.add(this.leave, this);
    },
    leave:function(){
        game.state.start('menu',true);
    }
}