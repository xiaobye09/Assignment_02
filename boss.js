function BOSS(config){
    this.init=function(){
        //game window width height
        this.gameWidth = config.windowWidth;
        this.gameHeight = config.windowHeight;
        //boss
        this.boss = game.add.sprite(config.initPosX,config.initPosY,config.bossPic);
        this.boss.anchor.setTo(0.5,0.5);
        this.boss.speed = config.speed;
        this.boss.health=50;
        //animation
        if(config.isAnimation){
            let ani = config.animation;
            this.boss.animations.add(ani.name,ani.frame,ani.repeatPerSec,ani.isRepeat);
            this.boss.play(ani.name);
        }
        //physics
        game.physics.arcade.enable(this.boss);
        this.boss.body.collideWorldBounds=true;
        let size = config.physicSize;
        this.boss.body.setSize(size.width,size.height,size.offSetX,size.offSetY); 
        //weapon
        if(config.weapon!=null){
            this.weapon = config.weapon;
            this.weapon.bulletPool.trackSprite(this.boss);
        }
        //bgm
        if(config.expBgm!=null){
            this.expBgm=game.add.audio('expBgm');
        }
        if(config.gunFire!=null){
            this.gunFire = game.add.audio('gunFire');
            this.gunFire.volume = 0.1;
        }
        //tween
        this.rndMove = game.add.tween(this.boss);
        //loop
        this.moveLoop = game.time.events.loop(Phaser.Timer.SECOND*config.moveInterval,this.move,this);
        this.moveLoop.timer.start();
    }
    this.move = function(){
        if(!this.rndMove.isRunning){
            console.log('boss move');
            let marginX = this.boss.width/2;
            let marginY =this.boss.height/2;
            
            let x= game.rnd.integerInRange(marginX, this.gameWidth-marginX);
            let y = game.rnd.integerInRange(marginY,this.gameHeight/2)
            this.rndMove.to( { x:x,y:y }, 2000, Phaser.Easing.Exponential.Out, true);       
        }
    }
    this.hitBoss=function(boss,bullet){
        this.boss.health--;
        if(this.boss.health<=0){
            score=score+config.point;
            boss.kill();
            try{
                this.exp = game.add.sprite(boss.body.x,boss.body.y,config.expPic);
                this.exp.animations.add(config.expPic); 
                this.exp.play(config.expPic,16,false,true);

                game.camera.shake(0.05, 400);
                this.expBgm.play();
            }catch{};
        }
        bullet.kill();
    };
    this.nivoseHit=function(boss,nivose){
        
        this.boss.health=this.boss.health-0.01;
        if(this.boss.health<=0){
            boss.kill();
            score=score+config.point;
            try{
                this.exp = game.add.sprite(boss.body.x,boss.body.y,config.expPic);
                this.exp.animations.add(config.expPic); 
                this.exp.play(config.expPic,16,false,true);

                game.camera.shake(0.05, 400);
                this.expBgm.play();
            }catch{};
        }
    };

    /*this.fire = function(){
        if(this.boss.alive&&this.weapon!=null&&this.fireButton.isDown){
            try{
                if(this.weapon.fire())
                    this.gunFire.play();
            }catch{}
        }
    };*/
    /*this.hitPlayer=function(player,bullet){
        this.player.health--;
        if(this.player.health<=0){
            player.kill();
            try{
                this.exp = game.add.sprite(player.body.x,player.body.y,config.expPic);
                this.exp.animations.add(config.expPic); 
                this.exp.play(config.expPic,16,false,true);

                this.emitter.x=player.body.x+player.body.halfWidth;
                this.emitter.y=player.body.y+player.body.halfHeight;
                this.emitter.start(true,1000,null,20);
                game.camera.shake(0.01, 200);
                this.expBgm.play();
            }catch{};
        }
        bullet.kill();
    };*/
    /*this.bumpPlayer = function(player,enemy){
        //since player is sprite type if enemy is a group,player will always be the first parameter
        enemy.health--;
        
        enemy.kill();
        
        player.kill();
        //explosion
        try{
            this.exp = game.add.sprite(player.body.x,player.body.y,config.expPic);
            this.exp.animations.add(config.expPic); 
            this.exp.play(config.expPic,16,false,true);

            this.emitter.x=player.body.x+player.body.halfWidth;
            this.emitter.y=player.body.y+player.body.halfHeight;
            this.emitter.start(true,1000,null,20);
            game.camera.shake(0.01, 200);
            this.expBgm.play();
        }catch{}
    }*/
}