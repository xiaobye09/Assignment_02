var menuState ={
    preload:function(){},
    create:function(){
        game.stage.backgroundColor = '#008000';
        var button0 = game.add.button(game.world.centerX, game.world.centerY, 'btn', this.actionOnClick0, this, 1, 0, 0);
        button0.anchor.setTo(0.5,0.5);
        var text0 = game.add.text(game.world.centerX,game.world.centerY,'PLAY EASY',34);
        text0.anchor.setTo(0.5,0.5);

        var button1 = game.add.button(game.world.centerX, game.world.centerY+50, 'btn', this.actionOnClick1, this, 1, 0, 0);
        button1.anchor.setTo(0.5,0.5);
        var text1 = game.add.text(game.world.centerX,game.world.centerY+50,'PLAY HARD',34);
        text1.anchor.setTo(0.5,0.5);

        
        var button2 = game.add.button(game.world.centerX, game.world.centerY+100, 'btn', this.actionOnClick2, this, 1, 0, 0);
        button2.anchor.setTo(0.5,0.5);
        var text2 = game.add.text(game.world.centerX,game.world.centerY+100,'SPEC',34);
        text2.anchor.setTo(0.5,0.5);
    },
    actionOnClick0:function(){
        game.state.start('play',true);
    },
    actionOnClick1:function(){
        a = 2;
        game.state.start('play',true);
    },actionOnClick2:function(){
        game.state.start('spec',true);
    }
}