var playState = {
    preload: function() {},
    create: function() {
        score=0;
        this.background = game.add.tileSprite(0, 0, width, height, 'background');
        game.physics.setBoundsToWorld();
        //state
        this.state = regularEnemyState;
        //add background audio
        this.bgm0 = game.add.audio('bgm0');
        this.bgm0.loop = true; 
        this.bgm0.play();
        //add weapons
        this.enemyWeapon0 = new WEAPON(enemyWeapon0);
        this.enemyWeapon0.init();

        
        this.player0Weapon0 = new WEAPON(Weapon0);
        this.player0Weapon0.init();
        this.player0Weapon1 = new WEAPON(Weapon1);
        this.player0Weapon1.init();
        this.player0Weapon2 = new WEAPON(Weapon2);
        this.player0Weapon2.init();
        this.player0Weapon3 = new WEAPON(Weapon3);
        this.player0Weapon3.init();
        this.player0Nivose = new WEAPON(Weapon4);
        this.player0Nivose.init();
        
        this.player1Weapon0 = new WEAPON(Weapon0);
        this.player1Weapon0.init();
        this.player1Weapon1 = new WEAPON(Weapon1);
        this.player1Weapon1.init();
        this.player1Weapon2 = new WEAPON(Weapon2);
        this.player1Weapon2.init();
        this.player1Weapon3 = new WEAPON(Weapon3);
        this.player1Weapon3.init();
        this.player1Nivose = new WEAPON(Weapon4);
        this.player1Nivose.init();
        //add player
        this.players=[];

        this.rndWeapon();
        player0.nivose = this.player0Nivose;
        this.players.push(new PLAYER(player0));
        this.players[0].init();
        
        player1.nivose = this.player1Nivose;
        this.players.push(new PLAYER(player1));
        this.players[1].init();
        //add enemies    
        this.boss = new BOSS(boss);
        enemy0.weapon=this.enemyWeapon0;
        this.enemy0 =new ENEMY(enemy0);  
        this.enemy0.init();

        this.suicidePlane = new ENEMY(suicidePlane);
        this.suicidePlane.init();
        //keyboard
        this.pause = game.input.keyboard.addKey(Phaser.KeyCode.P);
        this.resume = game.input.keyboard.addKey(Phaser.KeyCode.R);
        this.back = game.input.keyboard.addKey(Phaser.KeyCode.B);
        this.start = game.input.keyboard.addKey(Phaser.KeyCode.X);
        //flag 
        this.fail = false;
        this.win = false;
    },
    update: function() {
        if(this.playersAlive())
        {   
            if(this.pause.isDown&&!this.win){
                this.pauseText = game.add.text(game.world.centerX,game.world.centerY,"PAUSE\nPRESS R RESUME",34);
                this.pauseText.stroke = '#000000';
                this.pauseText.align = 'center';                
                this.pauseText.anchor.setTo(0.5,0.5);
                game.paused = !game.paused;
            }
            this.stateController();
            //move background
            this.background.tilePosition.y+=1;
            //others
            
            this.enemy0.fire();

            this.players[0].move();
            this.players[1].move();
            

            
            if(this.state==bossState){
                if(this.boss.boss.alive){
                    this.players[1].fire(this.boss.boss);
                    this.players[0].fire(this.boss.boss);
                }else{
                    this.youWin();
                }
            }else if(this.state==regularEnemyState){
                let targets = this.autoAim();
                this.players[1].fire(targets[1]);
                this.players[0].fire(targets[0]);
            }
                
            this.PhysicDetect();
        }else{
            this.youLose();
        }
    },
    pauseUpdate:function(){
        if(this.resume.isDown){
            this.pauseText.destroy();
            game.paused = !game.paused;
        }
    },
    render:function(){
        game.debug.text( "score: "+score, 200, 20,'red' );
        game.debug.text( "player0's heath:"+this.players[0].player.health, 5, 20 );
        game.debug.text( "player0's skill: "+(this.players[0].nivose.bulletPool.fireLimit-this.players[0].nivose.bulletPool.shots), 5, 40);
        game.debug.text( "player0's heath:"+this.players[1].player.health, width-165, 20 );
        game.debug.text( "player0's skill: "+(this.players[1].nivose.bulletPool.fireLimit-this.players[1].nivose.bulletPool.shots), width-175, 40);
    },
    /************************************************** */
    /*****************game function**********************/
    /************************************************** */
    PhysicDetect:function(){
        let player0 = this.players[0];
        let suicidePlane = this.suicidePlane;
        let player1 = this.players[1];
        let enemy0 = this.enemy0;
        let player0Nivose = this.players[0].nivose.bulletPool;
        let player1Nivose = this.players[1].nivose.bulletPool;
        let player0Weapon = this.players[0].weapon.bulletPool;
        let player1Weapon = this.players[1].weapon.bulletPool;
        let enemy0Weapon = enemy0.weapon.bulletPool;
        let boss = this.boss;
        game.physics.arcade.overlap(player0Nivose.bullets, suicidePlane.enemys, suicidePlane.nivoseHit, null, suicidePlane);
        game.physics.arcade.overlap(player0Nivose.bullets, enemy0.enemys, enemy0.nivoseHit, null, enemy0);
        game.physics.arcade.overlap(player0Weapon.bullets, enemy0.enemys, enemy0.hitEnemy, null, enemy0);
        game.physics.arcade.overlap(player0Weapon.bullets, suicidePlane.enemys, suicidePlane.hitEnemy, null, suicidePlane);
        if(this.state==bossState){
            game.physics.arcade.overlap(player0Nivose.bullets, boss.boss, boss.nivoseHit, null, boss);
            game.physics.arcade.overlap(player0Weapon.bullets, boss.boss, boss.hitBoss, null, boss);
        }
            

            
        game.physics.arcade.overlap(player1Nivose.bullets, suicidePlane.enemys, suicidePlane.nivoseHit, null, suicidePlane);    
        game.physics.arcade.overlap(player1Nivose.bullets, enemy0.enemys, enemy0.nivoseHit, null, enemy0);
        game.physics.arcade.overlap(player1Weapon.bullets, enemy0.enemys, enemy0.hitEnemy, null, enemy0);
        game.physics.arcade.overlap(player1Weapon.bullets, suicidePlane.enemys, suicidePlane.hitEnemy, null, suicidePlane);
        if(this.state==bossState){
            game.physics.arcade.overlap(player1Weapon.bullets, boss.boss, boss.hitBoss, null, boss);
            game.physics.arcade.overlap(player1Nivose.bullets, boss.boss, boss.nivoseHit, null, boss);
        }
            

        game.physics.arcade.overlap(suicidePlane.enemys,player1.player,player1.bumpPlayer,null,player1);    
        game.physics.arcade.overlap(suicidePlane.enemys,player0.player,player0.bumpPlayer,null,player0);    
        game.physics.arcade.overlap(enemy0Weapon.bullets,player0.player,player0.hitPlayer,null,player0);    
        game.physics.arcade.overlap(enemy0Weapon.bullets,player1.player,player1.hitPlayer,null,player1);
        game.physics.arcade.overlap(enemy0.enemys,player0.player,player0.bumpPlayer,null,player0);    
        game.physics.arcade.overlap(enemy0.enemys,player1.player,player1.bumpPlayer,null,player1);
    },
    playersAlive:function(){
        return this.players[0].player.alive||this.players[1].player.alive;
    },
    youLose:function(){
        if(this.fail===false){
            game.time.events.removeAll()

            this.loseText = game.add.text(game.world.centerX,game.world.centerY,"YOU LOSE!!\nPRESS X RESTART\nPRESS B BACK",34);
            this.loseText.align = 'center';                
            this.loseText.anchor.setTo(0.5,0.5);
            this.bgm0.destroy();
            this.fail = true;
        }else{
            if(this.start.isDown){
                game.state.restart();
            }else if(this.back.isDown){
                this.loseText.destroy();
                game.state.start('menu');
            }
        }
    },
    stateController:function(){
        if(this.enemy0.expired()&&this.suicidePlane.expired()&&this.state==regularEnemyState){
            this.state = waitState;
            console.log('wait state')
        }else if(this.state==waitState){
            //release boss;
            this.state = bossState;
            this.boss.init();
            console.log('boss start');
        }
    },
    autoAim:function(){
        let player0 = this.players[0].player;
        let player1 = this.players[1].player;
        let e0 = this.enemy0.pickClosestTo(player0);
        let e1 = this.suicidePlane.pickClosestTo(player0);
        let math = game.math;
        let arr = [];
        if(e0!=null){
            var len0=math.distance(e0.centerX,e0.centerY,player0.centerX,player0.centery);
        }
        if(e1!=null){
            var len1=math.distance(e1.centerX,e1.centerY,player0.centerX,player0.centery);
        }
        if(e0==null&&e1==null){
            arr.push(null);
        }else{
            if(e0&&e1){
                if(len0<len1){
                    arr.push(e0);
                }else{
                    arr.push(e1);
                }
            }else if(e0==null){
                arr.push(e1);
            }else{
                arr.push(e0);
            }
        }
        

        let e2 = this.enemy0.pickClosestTo(player1);
        let e3 = this.suicidePlane.pickClosestTo(player1);
    
        if(e2!=null){
            var len2=math.distance(e2.centerX,e2.centerY,player1.centerX,player1.centery);
        }
        if(e3!=null){
            var len3=math.distance(e3.centerX,e3.centerY,player1.centerX,player1.centery);
        }
        if(e2==null&&e3==null){
            arr.push(null);
        }else{
            if(e2&&e3){
                if(len2<len3){
                    arr.push(e2);
                }else{
                    arr.push(e3);
                }
            }else if(e2==null){
                arr.push(e3);
            }else{
                arr.push(e2);
            }
        }
        return arr;
    },
    youWin:function(){
        if(this.win===false){
            game.time.events.removeAll()

            this.winText = game.add.text(game.world.centerX,game.world.centerY,"YOU WIN!!\n"+"GOT "+score+" POINTS!!"+"\nPRESS X RESTART\nPRESS B BACK",34);
            this.winText.align = 'center';                
            this.winText.anchor.setTo(0.5,0.5);
            this.bgm0.destroy();
            this.win = true;
        }else{
            if(this.start.isDown){
                game.state.restart();
            }else if(this.back.isDown){
                this.winText.destroy()
                game.state.start('menu');
            }
        }
    },
    rndWeapon:function(){
        let rnd = game.rnd.integerInRange(0,3);
        if(rnd==0){
            player0.weapon = this.player0Weapon0;
        }else if(rnd==1){
            player0.weapon = this.player0Weapon1;
        }else if(rnd == 2){
            player0.weapon = this.player0Weapon2;
        }else if(rnd == 3){
            player0.weapon = this.player0Weapon3;
        }
        rnd = game.rnd.integerInRange(0,3);
        if(rnd==0){
            player1.weapon = this.player1Weapon0;
        }else if(rnd==1){
            player1.weapon = this.player1Weapon1;
        }else if(rnd == 2){
            player1.weapon = this.player1Weapon2;
        }else if(rnd == 3){
            player1.weapon = this.player1Weapon3;
        }
    }
};

