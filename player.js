function PLAYER(config){
    this.init=function(){
        //game window width height
        this.gameWidth = config.windowWidth;
        this.gameHeight = config.windowHeight;
        //player
        this.player = game.add.sprite(config.initPosX,config.initPosY,config.playerPic);
        this.player.anchor.setTo(0.5,0.5);
        this.player.speed = config.speed;
        this.player.health=3;
        //animation
        if(config.isAnimation){
            let ani = config.animation;
            this.player.animations.add(ani.name,ani.frame,ani.repeatPerSec,ani.isRepeat);
            this.player.play(ani.name);
        }
        //physics
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds=true;
        let size = config.physicSize;
        this.player.body.setSize(size.width,size.height,size.offSetX,size.offSetY); 
        //button
        this.nivoseButton = game.input.keyboard.addKey(config.nivoseButton);
        this.fireButton = game.input.keyboard.addKey(config.fireButton);
        this.cursors = game.input.keyboard.addKeys(config.cursors);
        //weapon
        if(config.weapon!=null){
            this.weapon = config.weapon;
            this.weapon.bulletPool.trackSprite(this.player);
        }
        //nivose
        if(config.nivose!=null){
            this.nivose =config.nivose;
            this.nivose.bulletPool.trackSprite(this.player);             
        }
        //particle emitter
        if(config.emitterPic!=null){
            this.emitter=game.add.emitter(0,0,20);
            this.emitter.makeParticles(config.emitterPic);
            this.emitter.setYSpeed(-50,50);
            this.emitter.setXSpeed(-50,50);
            this.emitter.setScale(1,0,1,0,1000);
            this.emitter.gravity =0;
        }
        //bgm
        if(config.expBgm!=null){
            this.expBgm=game.add.audio('expBgm');
        }
        if(config.gunFire!=null){
            this.gunFire = game.add.audio('gunFire');
            this.gunFire.volume = 0.1;
        }
    }
    this.move = function(){
        let player = this.player;
        if(player.alive){
            let cursor = this.cursors;
            if(cursor.up.isDown){
                player.y-=player.speed;
            }
            if(cursor.down.isDown){
                player.y+=player.speed;
            }
            if(cursor.left.isDown){
                player.x-=player.speed;
            }
            if(cursor.right.isDown){
                player.x+=player.speed;
            }
        }
    }

    this.fire = function(target){
        if(this.player.alive&&this.weapon!=null&&this.fireButton.isDown){
            try{
                if(this.weapon.fire(target)){
                    this.gunFire.play();
                }
                    
            }catch{}
        }
        if(this.player.alive&&this.nivose!=null&&this.nivoseButton.isDown){
            try{
                console.log('fire nivose');
                this.nivose.fire();
            }catch{}
        }
    };
    this.hitPlayer=function(player,bullet){
        this.player.health--;
        if(this.player.health<=0){
            player.kill();
            try{
                this.exp = game.add.sprite(player.body.x,player.body.y,config.expPic);
                this.exp.animations.add(config.expPic); 
                this.exp.play(config.expPic,16,false,true);

                this.emitter.x=player.body.x+player.body.halfWidth;
                this.emitter.y=player.body.y+player.body.halfHeight;
                this.emitter.start(true,1000,null,20);
                game.camera.shake(0.01, 200);
                this.expBgm.play();
            }catch{};
        }
        bullet.kill();
    };
    this.bumpPlayer = function(player,enemy){
        //since player is sprite type if enemy is a group,player will always be the first parameter
        enemy.health--;
        
        enemy.kill();
        player.health=0;
        player.kill();
        //explosion
        try{
            this.exp = game.add.sprite(player.body.x,player.body.y,config.expPic);
            this.exp.animations.add(config.expPic); 
            this.exp.play(config.expPic,16,false,true);

            this.emitter.x=player.body.x+player.body.halfWidth;
            this.emitter.y=player.body.y+player.body.halfHeight;
            this.emitter.start(true,1000,null,20);
            game.camera.shake(0.01, 200);
            this.expBgm.play();
        }catch{}
    };
}
/*config ={
    windowWidth:number,
    windowHeight:number,
    initPosX:number,                width/2            
    initPosY:number,                height*4/5
    playerPic:string,
    isAnimation:boolean,
    animation:{
        name:string,
        frame:frame,                
        repeatPerSec:number,
        isRepeat:boolean
    },
    physicSize:{
        width:number,            40
        height:number,           40
        offSetX:number,          12
        offSetY:number           12
    },
    speed:number,
    fireButton:keycode,
    cursors:{
        up:keycode,
        down:keycode,
        left:keycode,
        right:keycode
    }

}*/
